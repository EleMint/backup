package main

import (
	"backup/pkg/notify"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
)

func init() {
	log.SetFormatter(&log.TextFormatter{})
	log.SetOutput(os.Stdout)
}

func main() {
	log.Println("backup")

	done := notify.ListenForInterrupt()
	ticker := time.NewTicker(3 * time.Second)

	for {
		select {
		case <-done:
			log.Println("exiting")
			os.Exit(0)
		case <-ticker.C:
			log.Println("tick")
		}
	}
}
