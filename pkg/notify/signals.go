package notify

import (
	"os"
	"os/signal"
	"syscall"
)

func ListenForInterrupt() <-chan bool {
	signalCh := make(chan os.Signal, 1)

	signal.Notify(
		signalCh,
		syscall.SIGINT,
		syscall.SIGTERM,
	)

	doneCh := make(chan bool, 1)

	go notifyInterrupt(signalCh, doneCh)

	return doneCh
}

func notifyInterrupt(signalCh chan os.Signal, doneCh chan<- bool) {
	<-signalCh
	os.Stdout.WriteString("\n")
	doneCh <- true
}
