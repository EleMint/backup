package environ

import (
	"errors"
	"os"
)

var ErrNotFound = errors.New("key not found in environment")

func Get(key string) (value string, err error) {
	value, found := os.LookupEnv(key)
	if !found {
		err = ErrNotFound
	}
	return
}

func GetDefault(key string, def string) string {
	value, found := os.LookupEnv(key)
	if !found {
		return def
	}
	return value
}
